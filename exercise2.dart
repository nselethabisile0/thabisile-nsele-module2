//"TODO:
//Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;
//a) Sort and print the apps by name;
//b) Print the winning app of 2017 and the winning app of 2018.;
//c) the Print total number of apps from the array."

void main() {
  List<String> mtnapps = [
    'FNB ',
    'Snapscan',
    'Live inspect',
    ' Wundrop ',
    'Domestly',
    ' Shyft ',
    'KhulanEcosystem',
    ' Naked Insurance ',
    'EasyEquities ',
    'Edtech'
  ];

  // Sort and print by name
  print(
      'List of winning apps of the MTN Business App of the Year Awards since 2012 :');
  mtnapps.sort();
  print(mtnapps);

  //winning app of 2017 and the winning app of 2018
  print('Winning app of 2017 was : ${mtnapps[1]}');

  print('And the winning app of 2018 was : ${mtnapps[7]} :');

  // total number of apps
  print('Total number of apps is : ${mtnapps.length}');
}
