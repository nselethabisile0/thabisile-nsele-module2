// "TODO:
// Write a basic program that stores and then prints the following
// data: Your name, favorite app, and city;
// "
void main() {
  String name = "Thabisile Nsele";
  String favoriteApp = "FNB";
  String city = "Cape Town";
  printStatement(name, favoriteApp, city);
}

void printStatement(String name, String favoriteApp, String city) {
  print(
      'Hello, MTN App Academy! My name is $name, my favorite app is $favoriteApp and i am from the city of $city.');
}
